# Script Manager
The Script Manager is a Sanic web application that allows you to manage and run Python scripts. This application provides RESTful endpoints for starting jobs, checking their status, and retrieving logs and output files.

# Prerequisites
Docker and Docker Compose installed on your machine.
Basic understanding of Python scripts.

# Getting Started
1. Clone the Repository
Clone the repository containing the application code to your local machine:

```
git clone https://gitlab.com/virtuele-gemeente-assistent/script.git
cd script
```
2. Build the Docker Image
Navigate to the directory containing your Dockerfile and run the following command to build the Docker image:

```
docker-compose build
```

3. Run the Sanic Application
To start the Sanic server, run:

```
docker-compose run script server
```

This command will start the Sanic server on port 8000, mapped to port 8022 on your host machine.

4. Access the Application
Open your browser or use a tool like curl or Postman to interact with the application at:

```
http://localhost:8022
```

# API Endpoints
## 1. List All Jobs
GET /

This endpoint returns an overview of all jobs with their statuses.

### Example Request:

```
curl http://localhost:8022/
```

### Example Response:

```
{
  "job1": {
    "status": "completed"
  },
  "job2": {
    "status": "running"
  }
}
```

## 2. Run a Job
GET /<jobname>

This endpoint starts a new job with the specified job name. You can also provide the script name and arguments.

### Example Request:

```
curl "http://localhost:8022/job1?script=example_script.py&arg=value1&arg=value2"
```

### Example Response:

```
{
  "message": "Job job1 started",
  "jobname": "job1"
}
```

## 3. Check Job Status
GET /<jobname>

If no script arguments are provided, this endpoint checks the status of an existing job.

### Example Request:

```
curl http://localhost:8022/job1
```

### Example Response:

```
{
  "status": "running"
}
```

## 4. Cancel a Job
POST /<jobname>/cancel

This endpoint cancels a running job.

### Example Request:

```
curl -X POST http://localhost:8022/job1/cancel
```

### Example Response:
```
Job job1 cancelled
```

## 5. Get Job Logs
GET /<jobname>/logs

This endpoint retrieves the logs of a specific job.

### Example Request:

```
curl http://localhost:8022/job1/logs
```

### Example Response:

```
{
  "log": [
    "Starting job 'job1' with command: python example_script.py value1 value2",
    "Job 'job1' completed successfully."
  ]
}
```

## 6. Get Job Files
GET /<jobname>/files

This endpoint retrieves the files generated by the job.

### Example Request:

```
curl http://localhost:8022/job1/files
```

### Example Response:

```
{
  "files": [
    "output.txt",
    "log.txt"
  ]
}
```

# Notes
- Make sure to replace example_script.py with the actual script you wish to run.
- Scripts should be placed in the appropriate directory as specified in your docker-compose.yml.
- Adjust the host and port in the application if needed.