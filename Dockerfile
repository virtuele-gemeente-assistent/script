FROM python:3.8-slim

WORKDIR /usr/local/src/scripts

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy scripts into the container
COPY /src/scripts /usr/local/src/scripts

COPY ./app /usr/local/src/app

COPY ./bin/script /usr/local/bin/script
RUN chmod +x /usr/local/bin/script

ENV PYTHONPATH="${PYTHONPATH}:/usr/local/src/scripts:/usr/local/src/app"

ENTRYPOINT ["/usr/local/bin/script"]

# aiofiles==23.2.1
# anyio==4.0.0
# certifi==2023.7.22
# h11==0.14.0
# html5tagger==1.3.0
# httpcore==0.18.0
# httptools==0.6.1
# httpx==0.25.0
# idna==3.4
# iniconfig==2.0.0
# multidict==6.0.4
# packaging==23.2
# pluggy==1.3.0
# pytest==7.4.3
# sanic==23.6.0
# sanic-routing==23.6.0
# sanic-testing==23.6.0
# sentry-sdk==1.33.0
# sniffio==1.3.0
# tracerite==1.1.0
# typing_extensions==4.8.0
# ujson==5.8.0
# urllib3==2.0.7
# uvloop==0.19.0
# websockets==12.0