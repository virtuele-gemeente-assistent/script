import argparse
import logging
import os
import sys
from typing import Any, Dict, List
from ruamel.yaml import YAML, YAMLError

from jsonschema import validate
from jsonschema.exceptions import SchemaError, ValidationError

from readers.yaml_file_reader import FileReader, YamlFileReader

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

schema = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "type": "object",
    "properties": {
        "story_id": {
            "type": "string",
            "minLength": 1
        },
        "story_name": {
            "type": "string",
            "minLength": 1
        },
        "metadata": {
            "type": "object",
            "properties": {
                "description": {
                    "type": "string"
                }
            },
            "required": ["description"]
        },
        "conversation": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "enum": ["default"]
                },
                "input": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "messages": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "output": {
                    "type": "object",
                    "properties": {
                        "responses": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "text": {
                                        "type": "array",
                                        "items": {
                                            "type": "string"
                                        }
                                    },
                                    "buttons": {
                                        "type": "array",
                                        "items": {
                                            "type": "object",
                                            "properties": {
                                                "title": {
                                                    "type": "string"
                                                },
                                                "payload": {
                                                    "type": "string"
                                                }
                                            },
                                            "required": ["title", "payload"]
                                        }
                                    }
                                },
                                "required": ["text"]
                            }
                        },
                        "context": {
                            "type": "object",
                            "properties": {
                                "utternaam": {
                                    "type": "boolean"
                                },
                                "product": {
                                    "type": "string"
                                },
                                "namens": {
                                    "type": "string"
                                },
                                "action": {
                                    "type": "string"
                                }
                            }
                        },
                        "statistics": {
                            "type": "object",
                            "properties": {
                                "upl": {
                                    "type": "string"
                                },
                                "performance": {
                                    "type": "string"
                                }
                            },
                        }
                    },
                    "required": ["responses"]
                },
                "expected_responses": {
                    "type": "array",
                    "items": {
                        "anyOf": [
                            {"type": "string"},
                            {"type": "object"}
                        ]
                    }
                },
                "suggestion": {
                    "type": "string"
                },
                "suggestions-import": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "chain": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string"
                            },
                            "input": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                }
                            },
                            "output": {
                                "type": "object",
                                "properties": {
                                    "responses": {
                                        "type": "array",
                                        "items": {
                                            "type": "object",
                                            "properties": {
                                                "text": {
                                                    "type": "array",
                                                    "items": {
                                                        "type": "string"
                                                    }
                                                }
                                            },
                                            "required": ["text"]
                                        }
                                    },
                                    "context": {
                                        "type": "object",
                                        "properties": {
                                            "utternaam": {
                                                "type": "boolean"
                                            },
                                            "product": {
                                                "type": "string"
                                            },
                                            "namens": {
                                                "type": "string"
                                            },
                                            "action": {
                                                "type": "string"
                                            }
                                        }
                                    },
                                    "statistics": {
                                        "type": "object",
                                        "properties": {
                                            "upl": {
                                                "type": "string"
                                            },
                                            "performance": {
                                                "type": "string"
                                            }
                                        },
                                    }
                                },
                                "required": ["responses"]
                            }
                        },
                        "required": ["name", "input", "output"]
                    }
                }
            },
            "anyOf": [
                {
                    "required": ["input"]
                },
                {
                    "required": ["messages"]
                },
                {
                    "required": ["output"]
                },
                {
                    "required": ["expected_responses"]
                }
            ]
        }
    },
    "required": ["story_id", "story_name", "metadata", "conversation"]
}
class DirectoryProcessor:
    def __init__(self, file_reader: FileReader):
        self.file_reader = file_reader
        self.all_valid = True  # Flag to track overall validity

    def process_directories(self, directories: List[str], story_names: List[str], filenames: List[str]):
        for directory in directories:
            for root, _, files in os.walk(directory):
                for file in files:
                    if (file.endswith('.yaml') or file.endswith('.yml')) and (not filenames or file in filenames):
                        file_path = os.path.join(root, file)
                        if self.validate_yaml(file_path):
                            data = self.file_reader.read(file_path)
                            if self.should_process_file(data, story_names):
                                self.process_stories(data, file_path)
                        else:
                            logger.error("Skipping file '%s' due to YAML error...", file_path)
                            self.all_valid = False  # Mark as invalid if there is a YAML error
        
        # Print result after processing all directories
        if self.all_valid:
            logger.info("All files are valid...")

    def should_process_file(self, data: List[Dict[str, Any]], story_names: List[str]) -> bool:
        if not story_names:
            return True
        for story in data:
            if story.get('story_name', '') in story_names:
                return True
        return False

    def remove_duplicate_buttons(self, story: Dict[str, Any]):

        def remove_duplicates(buttons: List[Dict[str, str]]) -> List[Dict[str, str]]:
            unique_buttons = []
            for button in buttons:
                if button not in unique_buttons:
                    unique_buttons.append(button)
            return unique_buttons

        def process_output(output: Dict[str, Any]):
            if 'responses' in output:
                for response in output['responses']:
                    if 'buttons' in response:
                        response['buttons'] = remove_duplicates(response['buttons'])
        
        def process_conversation(conversation: Dict[str, Any]):
            if 'output' in conversation:
                process_output(conversation['output'])
            
            if 'chain' in conversation:
                for chained_conversation in conversation['chain']:
                    process_output(chained_conversation['output'])

        process_conversation(story['conversation'])

    def update_stories_in_file(self, data: List[Dict[str, Any]], file_path: str):
        """Update stories in file by removing duplicate buttons, keeping the order, and adding blank lines."""
        # Write updated data back to the file with the correct format
        with open(file_path, 'w') as file:
            yaml = YAML()
            yaml.explicit_start = False
            yaml.explicit_end = False
            yaml.default_flow_style = False
            yaml.indent(mapping=2, sequence=2, offset=0)  # Adjust indentation for lists
            yaml.width = float('inf')  # Disable line wrapping for strings

            # Write YAML header
            file.write('%YAML 1.1\n')
            file.write('---\n')

            # Make sure to add a blank line between stories
            for i, story in enumerate(data):
                yaml.dump([story], file)
                if i < len(data) - 1:  # Add a blank line between stories
                    file.write('\n')

    def validate_story(self, story: Dict[str, Any], file_path: str):
        try:
            validate(story, schema)
        except ValidationError as e:
            logger.error("Validation error in file '%s': %s", file_path, e.message)
            logger.error("Failed story name: %s", story['story_name'])
            logger.error("Failed line number: %s", e.absolute_path)
            logger.error("Failed value: %s", e.instance)
            logger.error("Failed schema: %s", e.schema)
            self.all_valid = False  # Mark as invalid if there is a validation error
        except SchemaError as e:
            logger.error("Schema error in file '%s': %s", file_path, e.message)
            self.all_valid = False  # Mark as invalid if there is a schema error

    def validate_yaml(self, file_path: str) -> bool:
        try:
            with open(file_path, 'r') as file:
                yaml = YAML()
                yaml.load(file)
            return True
        except YAMLError as e:
            logger.error("YAML error in file '%s': %s", file_path, e)
            return False
            
    def process_stories(self, data: List[Dict[str, Any]], file_path: str):
        for story in data:
            logger.info("Story: %s", story['story_name'])
            logger.info("_" * 50)
            self.remove_duplicate_buttons(story)
            self.validate_story(story, file_path)

        # Update the stories in the file, ensuring order and adding blank lines
        self.update_stories_in_file(data, file_path)

def main(directories: List[str], story_names: List[str], filenames: List[str]):
    if not directories:
        logger.info("No directories provided... Using default directories.")
        simple_stories_path = "simple-stories/functional_tests"
        logger.info("Using default directory: %s", simple_stories_path)
        directories = [simple_stories_path]
        
    yaml_reader = YamlFileReader()
    processor = DirectoryProcessor(yaml_reader)
    processor.process_directories(directories, story_names, filenames)

    # Return an appropriate exit code
    return 0 if processor.all_valid else 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process directories for specific stories and filenames.")
    parser.add_argument("--directories", nargs="+", help="Directories to process.")
    parser.add_argument("--story-names", nargs="+", help="Specific story names to process.")
    parser.add_argument("--filenames", nargs="+", help="Specific filenames to process.")

    args = parser.parse_args()
    sys.exit(main(args.directories or [], args.story_names or [], args.filenames or []))