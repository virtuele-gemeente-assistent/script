#!/usr/local/bin/python
import os
import xml.etree.ElementTree as ET
import sys
from collections import defaultdict

def find_story_suggestion_combinations_in_decision_table(xml_file):
    indexes = {}
    tree = ET.parse(xml_file)
    root = tree.getroot()
    return_dict = defaultdict(list)

    # Find 'decisionTable' elements
    ns = {"default": "https://www.omg.org/spec/DMN/20191111/MODEL/"}
    decision_tables = root.findall(".//default:decisionTable", namespaces=ns)
    for decision_table in decision_tables:
        # Find all indexes of the 'output' element within the 'decisionTable'
        output_elements = decision_table.findall(".//default:output", namespaces=ns)
        for output_element in output_elements:
            # Check if the 'decisionTable' has attribute 'name' equal to 'suggestion'
            if output_element.get('name') == 'suggestion':
                index = output_elements.index(output_element)
                indexes['suggestion'] = index
            elif output_element.get('name') == 'story_id':
                index = output_elements.index(output_element)
                indexes['story_id'] = index
        rule_elements = decision_table.findall(".//default:rule", namespaces=ns)
        for rule_element in rule_elements:
            outputentry_elements = rule_element.findall(".//default:outputEntry", namespaces=ns)
            storyid_text = None
            suggestion_text = None
            if 'story_id' in indexes:
                storyid_text = outputentry_elements[indexes['story_id']].find(".//default:text", namespaces=ns).text
                if storyid_text is not None:
                    storyid_text = storyid_text.strip('"')  # Remove double quotes from the key
                if 'suggestion' in indexes:
                    suggestion_text = outputentry_elements[indexes['suggestion']].find(".//default:text", namespaces=ns).text
                    if suggestion_text is not None:
                        suggestion_text = suggestion_text.strip('"')  # Remove double quotes from the value
                if storyid_text is not None and suggestion_text is not None:
                    return_dict[storyid_text].append(suggestion_text)

    return return_dict

def find_indexes_in_decision_table(xml_file):
    indexes = {}
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # Find 'decisionTable' elements
    ns = {"default":"https://www.omg.org/spec/DMN/20191111/MODEL/"}
    decision_tables = root.findall(".//default:decisionTable",namespaces=ns)
    for decision_table in decision_tables:
        # Find all indexes of the 'output' element within the 'decisionTable'
        output_elements = decision_table.findall(".//default:output",namespaces=ns)
        for output_element in output_elements:
            # Check if the 'decisionTable' has attribute 'name' equal to 'suggestion'
            if output_element.get('name') == 'suggestion':
                index = output_elements.index(output_element)
                indexes['suggestion'] = index
            elif output_element.get('name') == 'story_id':
                index = output_elements.index(output_element)
                indexes['story_id'] = index
        rule_elements = decision_table.findall(".//default:rule",namespaces=ns)
        for rule_element in rule_elements:
            outputentry_elements = rule_element.findall(".//default:outputEntry",namespaces=ns)
            storyid_text = None
            suggestion_text = None
            if 'story_id' in indexes:
                storyid_text = outputentry_elements[indexes['story_id']].find(".//default:text",namespaces=ns).text
            if 'suggestion' in indexes:
                suggestion_text = outputentry_elements[indexes['suggestion']].find(".//default:text",namespaces=ns).text
            if suggestion_text:
                print(f"{storyid_text},{suggestion_text}")
    return indexes

def process_folder(folder_path):
    # Check if the folder path is valid
    if not os.path.exists(folder_path):
        print(f"Error: Folder '{folder_path}' not found.")
        return

    xml_files = [file for file in os.listdir(folder_path) if file.endswith(".dmn")]

    if not xml_files:
        print(f"No DMN files found in the folder '{folder_path}'.")
        return

    for xml_file in xml_files:
        xml_file_path = os.path.join(folder_path, xml_file)
        indexes = find_indexes_in_decision_table(xml_file_path)

        if indexes:
            pass
            #print(f"Indexes of 'output' with attribute 'name' equals 'suggestion' in 'decisionTable' in '{xml_file}': {indexes}")
        else:
            print(f"No matching elements found in '{xml_file}'.")

if __name__ == "__main__":
    # Check if a folder path is provided as a command-line argument
    if len(sys.argv) != 2:
        # print("Usage: python script.py <folder_path>")
        process_folder("/var/data/gem/rasa/data/camunda-dmn/")
    else:
        folder_path = sys.argv[1]
        process_folder(folder_path)