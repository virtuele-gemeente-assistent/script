from ruamel.yaml import YAML
from io import StringIO
from find_suggestions import *
import os
from ruamel.yaml.scalarstring import PreservedScalarString, ScalarString



def update_document_with_ruamel(document, responses, suggestions):
    yaml = YAML()
    yaml.preserve_quotes = True
    document_data = yaml.load(document)
    responses_data = yaml.load(responses)

    updated_document_data = []
    for story in document_data:
        updated_story = {
            'story_id': story.get('story_id', ''),
            'story_name': story.get('story_name', ''),
            'metadata': story.get('metadata', {}),
            'conversation': []
        }
        
        # Initialize suggestion and suggestions-import once for the conversation
        conversation_suggestion_added = False
        conversation_suggestions_import_added = False

        for conv in story.get('conversation', []):
            input_messages = [msg for msg in conv.get('messages', [])]
            expected_responses = conv.get('expected_responses', [])
            
            conversation_item = {
                'name': 'default',
                'input': input_messages,
                'output': {
                    'responses': [],
                    'context': {},
                    'statistics': {}
                }
            }
            
            dynamic_context = {}
            dynamic_statistics = {}
            buttons = []
            individual_responses = []

            for response in expected_responses:
                utter_name = response.get('utter_name')
                
                if utter_name in responses_data['responses']:
                    if utter_name.startswith("utter_lo") and len(responses_data['responses'][utter_name]) > 1:
                        # First step in new e2e story format (in case of local utter, responsetext is uttername)
                        individual_responses.append(utter_name)
                        # individual_responses.append("!local " + responses_data['responses'][utter_name][-1].get('custom',{}).get('default',{})[0].get('text',{}))
                    else:
                        individual_responses.append(responses_data['responses'][utter_name][0].get('text',{}))

                    for r in responses_data['responses'][utter_name]:
                        # Handle dynamic context and statistics if present
                        if 'custom' in r:
                            dynamic_context.update(r['custom'].get('context', {}))
                            dynamic_statistics.update(r['custom'].get('statistics', {}))
                        
                        # Check if 'buttons' are present in the response and process them
                        if 'buttons' in r:
                            for button in r['buttons']:
                                buttons.append({
                                    'title': button.get('title', ''),
                                    'payload': button.get('payload', '')
                                })

            # Use PreservedScalarString for the response text to keep formatting
            for res in individual_responses:
                res = ScalarString(res)

            # Construct the output part of the conversation item
            if buttons:
                conversation_item_output = {
                    'responses': [{
                        'text': individual_responses,  # Assign the list of responses here
                        'buttons': buttons,
                    }],
                    'context': dynamic_context,
                    'statistics': dynamic_statistics
                }
            else:
                conversation_item_output = {
                    'responses': [{
                        'text': individual_responses,  # Assign the list of responses here
                    }],
                    'context': dynamic_context,
                    'statistics': dynamic_statistics
                }

            # Append the constructed output to the conversation item
            conversation_item['output'] = conversation_item_output

            
            updated_story['conversation'].append(conversation_item)
            
            # Add suggestions to the conversation_item only once for the conversation
            if not conversation_suggestion_added and not conversation_suggestions_import_added:
                story_id = updated_story['story_id']
                if story_id in suggestions:
                    suggestions_list = suggestions[story_id]
                    if suggestions_list:
                        # Remove duplicates from suggestions_list
                        suggestions_list = list(set(suggestions_list))
                        # Find the most common suggestion and add it to 'suggestion-import'
                        most_common_suggestion = max(suggestions_list, key=suggestions_list.count)
                        conversation_item['suggestion'] = most_common_suggestion
                        conversation_item['suggestions-import'] = [s for s in suggestions_list]
                        
                        # Mark that suggestions have been added to this conversation
                        conversation_suggestion_added = True
                        conversation_suggestions_import_added = True
        
        # Add comment for stories with multiple "input" and "output" keys in order to manually inspect suggestions.
        if len(updated_story['conversation']) > 1:
            updated_story['comment'] = '#TODO check suggestions'
            
        updated_document_data.append(updated_story)
    
    # Return the YAML output as a string
    output_stream = StringIO()
    yaml.dump(updated_document_data, output_stream)
    return output_stream.getvalue()
# Folder containing document files
document_folder = '/usr/local/src/end2end/test_data/functional_tests/'

# Responses file path
responses_file_path = '/usr/local/src/rasa/domain/responses.yml'

# Output folder path
output_folder = './output/'

# Find suggestions using the find_suggestions function
suggestions = {}
xml_files = [file for file in os.listdir("/usr/local/src/rasa/data/camunda-dmn/") if file.endswith(".dmn")]

for xml_file in xml_files:
    xml_file_path = os.path.join("/usr/local/src/rasa/data/camunda-dmn/", xml_file)
    suggestions.update(find_story_suggestion_combinations_in_decision_table(xml_file_path))

# Loop through all document files in the folder
for document_file in os.listdir(document_folder):
    if document_file.endswith("algemeen-overig.yml"):
        print("processing file: ", document_file)
        document_file_path = os.path.join(document_folder, document_file)

        # Read the document content
        with open(document_file_path, 'r') as doc_file, open(responses_file_path, 'r') as resp_file:
            document_content_ruamel = doc_file.read()
            responses_content_ruamel = resp_file.read()

        # Uitvoeren van de functie en krijgen van de output
        updated_document_ruamel = update_document_with_ruamel(document_content_ruamel, responses_content_ruamel, suggestions)

        # Create an output file path based on the input document file name
        output_file_name = os.path.splitext(document_file)[0] + '_updated.yml'
        output_file_path = os.path.join(output_folder, output_file_name)

        # Write the updated document to the output file
        with open(output_file_path, 'w') as output_file:
            output_file.write(updated_document_ruamel)
