import yaml
from typing import Any, Dict, List

class FileReader:
    def read(self, file_path: str) -> List[Dict[str, Any]]:
        """Read data from a file. Must be implemented by subclasses."""
        raise NotImplementedError("Subclasses must implement this method")

    def write(self, file, data: List[Dict[str, Any]]):
        """Write data to a file. Must be implemented by subclasses."""
        raise NotImplementedError("Subclasses must implement this method")

class YamlFileReader(FileReader):
    def read(self, file_path: str) -> List[Dict[str, Any]]:
        """Read and parse YAML file."""
        with open(file_path, 'r') as file:
            return yaml.safe_load(file)
    
    def write(self, file, data: List[Dict[str, Any]]):
        """Write data to a YAML file."""
        yaml.dump(data, file, default_flow_style=False, sort_keys=False)

