import argparse
import logging
import os
import sys
from typing import Any, Dict, List
from ruamel.yaml import YAML, YAMLError

from readers.yaml_file_reader import FileReader, YamlFileReader

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class DirectoryProcessor:
    def __init__(self, file_reader: FileReader):
        self.file_reader = file_reader
        self.all_valid = True  # Flag to track overall validity

    def process_directories(self, directories: List[str], story_names: List[str], filenames: List[str]):
        for directory in directories:
            for root, _, files in os.walk(directory):
                for file in files:
                    if (file.endswith('.yaml') or file.endswith('.yml')) and (not filenames or file in filenames):
                        file_path = os.path.join(root, file)
                        if self.validate_yaml(file_path):
                            logger.info("File '%s' is valid...", file_path)
                        else:
                            logger.error("Skipping file '%s' due to YAML error...", file_path)
                            self.all_valid = False  # Mark as invalid if there is a YAML error
        
        # Print result after processing all directories
        if self.all_valid:
            logger.info("All files are valid...")
        else:
            logger.error("Some files are invalid...")

    def validate_yaml(self, file_path: str) -> bool:
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                yaml = YAML()
                yaml.load(file)
            return True
        except UnicodeDecodeError as e:
            logger.error("Unicode decoding error in file '%s': %s", file_path, e)
            return False
        except YAMLError as e:
            # Enhanced error logging for YAML validation
            self.log_yaml_error(file_path, e)
            return False

    def log_yaml_error(self, file_path: str, error: YAMLError):
        # Log the YAML error with details including file path and line number
        if hasattr(error, 'problem_mark'):
            mark = error.problem_mark
            line = mark.line + 1  # Line numbers are zero-indexed
            column = mark.column + 1  # Columns are zero-indexed
            logger.error("YAML error in file '%s' at line %d, column %d: %s", file_path, line, column, error)
        else:
            logger.error("YAML error in file '%s': %s", file_path, error)

def main(directories: List[str], story_names: List[str], filenames: List[str]) -> int:
    if not directories:
        logger.info("No directories provided... Using default directories.")
        simple_stories_path = "simple-stories/functional_tests"
        domain_path = "domain"
        logger.info("Using default directory: %s and %s", simple_stories_path, domain_path)
        directories = [simple_stories_path, domain_path]
        
    yaml_reader = YamlFileReader()
    processor = DirectoryProcessor(yaml_reader)
    processor.process_directories(directories, story_names, filenames)
    
    if processor.all_valid:
        return 0  # Success
    else:
        return 1  # Fail

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process directories for specific stories and filenames.")
    parser.add_argument("--directories", nargs="+", help="Directories to process.")
    parser.add_argument("--story-names", nargs="+", help="Specific story names to process.")
    parser.add_argument("--filenames", nargs="+", help="Specific filenames to process.")

    args = parser.parse_args()
    sys.exit(main(args.directories or [], args.story_names or [], args.filenames or []))
