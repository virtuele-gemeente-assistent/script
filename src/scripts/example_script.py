import json
import os
import random

def generate_random_data():
    """Generates random data in dictionary format."""
    data = {
        "name": f"Item {random.randint(1, 100)}",
        "value": random.uniform(1.0, 100.0),
        "description": "This is a randomly generated item.",
        "tags": random.sample(["tag1", "tag2", "tag3", "tag4", "tag5"], k=random.randint(1, 3))
    }
    return data

def write_json_file(data, output_path):
    """Writes the data to a JSON file at the specified output path."""
    with open(output_path, 'w') as json_file:
        json.dump(data, json_file, indent=4)
    print(f"JSON file created at: {output_path}")

def main():
    # Get the current working directory
    cwd = os.getcwd()
    print(f"Current working directory: {cwd}")

    # Generate random data
    random_data = generate_random_data()

    # Define the output path for the JSON file
    output_path = os.path.join(cwd, "random_data.json")

    # Write the random data to a JSON file
    write_json_file(random_data, output_path)

if __name__ == "__main__":
    main()