import yaml
import aiohttp
import asyncio
import time
import os
import requests
import copy
from itertools import product
import datetime

http_session = requests.Session()

RASA_NLU_URL = "http://rasa-nlu:5005/model/parse"
RASA_WEBHOOK_URL = "http://rasa:5005/webhooks/test/webhook"

async def nlu_parse(sem, stats, session, message: str, max_retries = 3) -> str:
    """
    Send a message to Rasa asynchronously using aiohttp and get the response
    params:
        sem: Semaphore
        stats: list of statistics
        session: aiohttp session
        message: The message to send
        max_retries: The maximum number of retries
    return: The response
    example:
        >>> await nlu_parse("test_user", "Hello")
        /greet
    """

    async with sem:
        for attempt in range(max_retries):
            try:
                start_timer = datetime.datetime.now()
                async with session.post(
                RASA_NLU_URL,
                json={"text": message},
                timeout=aiohttp.ClientTimeout(total=1000)
            ) as response:
                    response_data = await response.json()
                    entities = [
                        f'"{entity["entity"]}": "{entity["value"]}",'
                        for entity in response_data["entities"]
                    ]

                    join_entities = '{' + " ".join(entities).rstrip(",") + '}'
                    result = f'/{response_data["intent"]["name"]}{join_entities}'
                    confidence = response_data["intent"]["confidence"]

                    end_timer = datetime.datetime.now()
                    duration = (end_timer - start_timer).total_seconds()
                    print(f"duration: {duration}")
                    average = stats[1]
                    newaverage = (average * stats[0] + duration) / (stats[0] + 1)
                    stats[0] += 1
                    stats[1] = newaverage

                    return result, confidence
            except Exception as e:
                print(f"Error parsing message (attempt {attempt + 1}/{max_retries}): {e}")
                await asyncio.sleep(1)  # Add a small delay before retrying

        # If all attempts fail, return None
        return None, None

async def core_parse(session, senderid_name, message, custom_data, max_retries = 3):
    """
    Send a message to Rasa asynchronously using aiohttp and get the response
    param senderid_name: The sender ID
    param message: The message from nlu_parse function (intent+entities)
    param custom_data: The custom data to send
    return: The response
    example:
        >>> await send_message_to_rasa("test_user", /inform{"location": "London"}, {"name": "test_user"})
        [{'recipient_id': 'test_user', 'text': 'Hello'}]
    """
    for attempt in range(max_retries):
        payload = {"sender": senderid_name, "message": message, "customData": custom_data}
        try:
            async with session.post(
                RASA_WEBHOOK_URL,
                json=payload,
                timeout=aiohttp.ClientTimeout(total=1000)
            ) as response:
                return await response.json()
        except Exception as e:
            print(f"Error parsing message (attempt {attempt + 1}/{max_retries}): {e}")
            print(f"message: {message}")
            print(f"payload: {payload}")

def delete_directory(directory):
    """
    Delete a directory.
    :param directory: The directory to delete.
    :return: None
    """
    try:
        # force delete directory  
        os.system(f"rm -rf {directory}")
    except Exception as e:
        print(f"Error deleting directory: {e}")

def load_yaml(file_path):
    """
    Load YAML data from a file.

    Parameters:
    - file_path (str): The path to the YAML file.

    Returns:
    - dict: The loaded YAML data.
    """
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)
    
def collect_stories(dir_path="test_data"):
    """
    Collect all stories from a directory.
    :param dir_path: The path to the directory.
    :return: list of stories
    """

    print("Collecting stories...")
    stories = []

    data_directories = os.listdir(dir_path)

    for directory in data_directories:
        for filename in os.listdir(f"{dir_path}/{directory}"):
            if filename.endswith(".yml"):
                path_to_file = f"{dir_path}/{directory}/{filename}"
                data = load_yaml(path_to_file)
                for story in data:
                    # use directory name and filename
                    obj = {
                        "directory_name": directory, 
                        "file_name": filename,
                        "story": story
                        }
                    stories.append(obj)

    print("Done collecting stories of length", len(stories))
    return stories

async def main():
    """
    Main function.
    This function is called when the script is run.
    """
    connector = aiohttp.TCPConnector(limit=100)
    sem = asyncio.Semaphore(5)
    dir_path = "../end2end/test_data/new_tests"
    target_directory = "../end2end/test_data_tmp"

    # delete target directory if it exists
    if os.path.exists(target_directory):
        delete_directory(target_directory)

    async with aiohttp.ClientSession(connector=connector) as session:
        # new code here
        stories = collect_stories(dir_path=dir_path)
        print("stories: ", stories)

if __name__ == "__main__":
    asyncio.run(main())