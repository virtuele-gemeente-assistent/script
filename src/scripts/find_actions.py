import os
import xml.etree.ElementTree as ET
import sys
from collections import defaultdict

def find_story_suggestion_combinations_in_decision_table(xml_file):
    indexes = {}
    tree = ET.parse(xml_file)
    root = tree.getroot()
    return_dict = defaultdict(list)

    # Find 'decisionTable' elements
    ns = {"default": "https://www.omg.org/spec/DMN/20191111/MODEL/"}
    decision_tables = root.findall(".//default:decisionTable", namespaces=ns)
    for decision_table in decision_tables:
        # Find all indexes of the 'output' element within the 'decisionTable'
        output_elements = decision_table.findall(".//default:output", namespaces=ns)
        for output_element in output_elements:
            # Check if the 'decisionTable' has attribute 'name' equal to 'suggestion'
            if output_element.get('name') == 'suggestion':
                index = output_elements.index(output_element)
                indexes['suggestion'] = index
            elif output_element.get('name') == 'story_id':
                index = output_elements.index(output_element)
                indexes['story_id'] = index
        rule_elements = decision_table.findall(".//default:rule", namespaces=ns)
        for rule_element in rule_elements:
            outputentry_elements = rule_element.findall(".//default:outputEntry", namespaces=ns)
            storyid_text = None
            suggestion_text = None
            if 'story_id' in indexes:
                storyid_text = outputentry_elements[indexes['story_id']].find(".//default:text", namespaces=ns).text
                if storyid_text is not None:
                    storyid_text = storyid_text.strip('"')  # Remove double quotes from the key
                if 'suggestion' in indexes:
                    suggestion_text = outputentry_elements[indexes['suggestion']].find(".//default:text", namespaces=ns).text
                    if suggestion_text is not None:
                        suggestion_text = suggestion_text.strip('"')  # Remove double quotes from the value
                if storyid_text is not None and suggestion_text is not None:
                    return_dict[storyid_text].append(suggestion_text)

    return return_dict

def find_indexes_in_decision_table(xml_file):
    indexes = {}
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # Find 'decisionTable' elements
    ns = {"default":"https://www.omg.org/spec/DMN/20191111/MODEL/"}
    decision_tables = root.findall(".//default:decisionTable",namespaces=ns)
    for decision_table in decision_tables:
        # Find all indexes of the 'output' element with responses within the 'decisionTable'
        output_elements = decision_table.findall(".//default:output",namespaces=ns)
        for output_element in output_elements:
            # Check if the 'decisionTable' has attribute 'name' equal to 'suggestion'
            index = output_elements.index(output_element)
            if output_element.get('name') == 'suggestion':
                indexes['suggestion'] = index
            elif output_element.get('name') == 'story_id':
                indexes['story_id'] = index
            elif output_element.get('name').startswith('response.'):
                indexlist = indexes.get('responses',[])
                indexlist.append(index)
                indexes['responses'] = indexlist
        rule_elements = decision_table.findall(".//default:rule",namespaces=ns)
        action_rules = 0
        check_livechat = 0
        simple_stories = 0
        only_livechat = 0
        for rule_element in rule_elements:
            outputentry_elements = rule_element.findall(".//default:outputEntry",namespaces=ns)
            storyid_text = None
            suggestion_text = None
            actionlist = []
            if 'story_id' in indexes:
                storyid_text = outputentry_elements[indexes['story_id']].find(".//default:text",namespaces=ns).text
            # if 'suggestion' in indexes:
            #     suggestion_text = outputentry_elements[indexes['suggestion']].find(".//default:text",namespaces=ns).text
            if 'responses' in indexes:
                for ri in indexes['responses']:
                    action_name = outputentry_elements[ri].find(".//default:text",namespaces=ns).text
                    if action_name and action_name.startswith('"action_'):
                        if action_name == '"action_check_livechat"':
                            check_livechat += 1
                        elif action_name == '"action_simple_stories"':
                            simple_stories += 1
                        actionlist.append(action_name)
            if actionlist:
                action_rules += 1
                if actionlist == ['"action_check_livechat"']:
                    only_livechat += 1
                # print(f"{storyid_text},{actionlist}")
        print(f"{os.path.basename(xml_file)},{len(rule_elements)},{action_rules},{check_livechat},{only_livechat},SS:{simple_stories},!SS:{len(rule_elements)-simple_stories}")
    return indexes

def process_folder(folder_path):
    # Check if the folder path is valid
    if not os.path.exists(folder_path):
        print(f"Error: Folder '{folder_path}' not found.")
        return

    xml_files = [file for file in os.listdir(folder_path) if file.endswith(".dmn")]

    if not xml_files:
        print(f"No DMN files found in the folder '{folder_path}'.")
        return

    for xml_file in xml_files:
        xml_file_path = os.path.join(folder_path, xml_file)
        indexes = find_indexes_in_decision_table(xml_file_path)

        if indexes:
            pass
            #print(f"Indexes of 'output' with attribute 'name' equals 'suggestion' in 'decisionTable' in '{xml_file}': {indexes}")
        else:
            pass
            # print(f"No matching elements found in '{xml_file}'.")

if __name__ == "__main__":
    # Check if a folder path is provided as a command-line argument
    if len(sys.argv) != 2:
        # print("Usage: python script.py <folder_path>")
        process_folder("/usr/local/src/rasa/data/camunda-dmn/")
    else:
        folder_path = sys.argv[1]
        process_folder(folder_path)