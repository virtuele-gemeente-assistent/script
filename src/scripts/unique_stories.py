#!/usr/local/bin/python
import os
import csv
import sys
import yaml
import logging


logger = logging.getLogger(__name__)

def process_folder(path):
        auto_files = os.listdir(path)
        # lf = open(path + '/utters.csv','w')
        # auto_files = ['verhuizen.yml']
        stories = []
        messagecount = 0
        combicount = 0
        combinations = {}
        toomanynodes = []
        for afile in auto_files:
            if afile.endswith('.yml'):
                logger.error(f"Training file: {afile}")
                with open(f"{path}/{afile}", 'r') as f:
                    fstories = yaml.safe_load(f)
                    for story in fstories:
                        story_id = f"{afile}/{story['story_id']}"
                        # logger.error(f"story_id: {story_id}")
                        if story['story_id'].endswith('-oud'):
                            break
                        previouslist = []
                        # if len(story['conversation']) > 1:
                        #     toomanynodes.append(story_id)
                        for intentcombi in story['conversation']['input_predicted']:
                            intentcombi_split = intentcombi.split('{')
                            intent = intentcombi_split[0][1:]
                            entities = "{"+intentcombi_split[1]
                            # logger.error(f"{intent}  {entities}")
                            storylist = combinations.get(intentcombi,[])
                            if len(storylist) == 0:
                                combinations[intentcombi] = storylist
                            if story_id not in storylist:
                                storylist.append(story_id)
                            # responselist = []
                            # for response in sround['input_predicted']:
                            #     responselist.append(response['utter_name'])
                                # lf.write(f"{story['story_id']}_{idx},{response['utter_name']}\n")
                            # for message in sround['messages']:
                            #     params = {
                            #         "text": message
                            #     }
                            #     ignore_entities = ['number']
                            #     ignore_values = ['municipality','land']
                                # parsed = session.post("http://rasa-nlu:5005/model/parse", json=params).json()
                                # intent = parsed.get('intent',{'name':''})['name']
                                # entities = {}
                                # for e in parsed.get('entities',[]):
                                #     entity = e['entity']
                                #     value = e['value']
                                #     if entity in ignore_entities:
                                #         continue
                                #     if entity in ignore_values:
                                #         value = entity
                                #     entities[entity] = value
                                # entities = dict(sorted(entities.items()))
                                # context = self._combine_context_string(responses,previouslist)

                                # combi = f"{context}{intent}{entities}"
                                # combilist = combinations.get(combi,None)
                                # if not combilist:
                                #     combilist = []
                                #     combinations[combi] = combilist
                                #     combicount += 1
                                # storydict = next((item for item in combilist if item["story_id"] == story['story_id']),None)
                                # if not storydict:
                                #     storydict = {"story_id":story['story_id'],"messages":[]}
                                #     combilist.append(storydict)
                                # storydict['responses'] = responselist
                                # for r in responselist:
                                #     lf.write("{},{}\n".format(story['story_name'],r))
                                # if previouslist:
                                #     storydict['previous_responses'] = previouslist
                                # storydict['messages'].append(message)
                                # messagecount += 1
                                # if combi not in combolist:
                                #     combolist.append(combi)

                                # loop = asyncio.get_running_loop()
                                # loop.close()
                                # logger.info(loop)
                                # parsed = loop.run_until_complete(interpreter.parse(message))
                                # loop.close()
                            # previouslist = responselist
                # break
        noconflictcount = 0
        conflictcount = 0
        fallbackcount = 0
        fallbackconflictcount = 0
        fallbacks = {}
        for k,v in combinations.items():
            if k.startswith('/nlu_fallback'):
                fallbacks[k] = v
                fallbackcount += 1
            elif len(v) > 1:
                conflictcount += 1
                logger.error(f"conflict on intentcombi {k}, with stories {v}")
            else:
                noconflictcount += 1

        for k,v in fallbacks.items():
            if len(v) > 1:
                fallbackconflictcount += 1
                logger.error(f"conflict on fallback {k}, with stories {v}")


        logger.error(f"combinations without conflict: {noconflictcount}, combinations with conflict: {conflictcount}, combination with fallback: {fallbackcount}, conflicts in fallback: {fallbackconflictcount}")

        for story_id in toomanynodes:
            logger.error(f"too many nodes in story: {story_id}")


        # lf.close()                        


def process_file(path):
    with open(path, newline='') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_ALL)
        for row in spamreader:
            print(row)

if __name__ == "__main__":
    # Check if a folder path is provided as a command-line argument
    if len(sys.argv) != 2:
        # print("Usage: python script.py <folder_path>")
        process_folder("/var/lib/data/rasa/temp/data/simple-stories/functional_tests")
    else:
        folder_path = sys.argv[1]
        process_folder(folder_path)