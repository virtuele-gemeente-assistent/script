import os
import copy
import yaml
import csv
import asyncio
import requests
import aiohttp
import pytz
import argparse
from datetime import datetime
from abc import ABC, abstractmethod
from collections import defaultdict
from typing import List, Dict, Any, Tuple
from itertools import product
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class FileReader(ABC):
    @abstractmethod
    def read(self, file_path: str) -> Any:
        pass

class FileWriter(ABC):
    @abstractmethod
    def write(self, file_path: str, data: Any) -> None:
        pass

class CorePredictor(ABC):
    @abstractmethod
    async def predict(
        self, session: aiohttp.ClientSession, sender_id: str, message: str,
        message_sequence: Tuple[str], custom_data: Dict[str, Any],
        story_id: str, file_name: str, max_retries: int = 3
    ) -> List[Dict[str, Any]]:
        pass

    @staticmethod
    @abstractmethod
    async def parse(response_data: List[Dict[str, Any]]) -> Tuple[List[str], List[List[Dict[str, str]]]]:
        pass
        
class YamlFileReader(FileReader):
    def read(self, file_path: str) -> Any:
        with open(file_path, 'r', encoding='utf-8') as file:
            return yaml.safe_load(file)

class CsvFileWriter(FileWriter):
    FIELDNAMES = [
        'sender_id', 'story_id', 'original_story_id', 'turn', 'message', 'predicted_intent',
        'confidence', 'original_expected_response', 'original_predicted_response', 'original_response_equal',
        'expected_response', 'predicted_response', 'expected_buttons', 'predicted_buttons',
        'file_name', 'expected_failure', 'response_success', 'buttons_success', 'timestamp'
    ]

    def write(self, file_path: str, data: List[Dict[str, Any]]) -> None:
        """Write the data to a CSV file and generate additional reports."""
        track_successful_ids, file_success_stats, original_story_success_stats = self._process_data(data)

        self._write_csv(file_path, data)
        self._write_confusion_matrix(file_path, track_successful_ids)
        self._write_success_rate(file_path, file_success_stats, '_success_rate_per_file_name.csv', 'file_name')
        self._write_success_rate(file_path, original_story_success_stats, '_success_rate_per_original_story_id.csv', 'original_story_id')

        logger.info(f"Results written to {file_path}")
        logger.info(f"Confusion matrix written to {file_path.replace('.csv', '_confusion.csv')}")
        logger.info(f"Success rate per file_name written to {file_path.replace('.csv', '_success_rate_per_file_name.csv')}")

    def _process_data(self, data: List[Dict[str, Any]]):
        """Process the data and generate additional reports.
        args:
            data: List[Dict[str, Any]]
        returns:
            Dict[str, bool], dict, dict
        """
        track_successful_ids = {}
        file_success_stats = defaultdict(lambda: {'total': 0, 'successful': 0})
        original_story_success_stats = defaultdict(lambda: {'total': 0, 'successful': 0})

        for row in data:
            sender_id = row['sender_id']
            response_success = row['response_success']
            track_successful_ids[sender_id] = response_success

            self._update_stats(file_success_stats, row['file_name'], response_success)
            self._update_stats(original_story_success_stats, row['original_story_id'], response_success)

        return track_successful_ids, file_success_stats, original_story_success_stats

    def _update_stats(self, stats_dict: dict, key: str, success: bool) -> None:
        """Update the stats dictionary with the success value.
        args:
            stats_dict: dict
            key: str
            success: bool
        """
        stats_dict[key]['total'] += 1
        if success:
            stats_dict[key]['successful'] += 1

    def _write_csv(self, file_path: str, data: List[Dict[str, Any]]) -> None:
        """Write the data to a CSV file.
        args:
            file_path: str
            data: List[Dict[str, Any]]
        """
        with open(file_path, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self.FIELDNAMES)
            writer.writeheader()
            for row in data:
                writer.writerow({k: str(v) for k, v in row.items() if k in self.FIELDNAMES})

    def _write_confusion_matrix(self, file_path: str, track_successful_ids: Dict[str, bool]) -> None:
        """Write the confusion matrix to a CSV file.
        args:
            file_path: str
            track_successful_ids: Dict[str, bool]
        """
        confusion_file = file_path.replace('.csv', '_confusion.csv')
        with open(confusion_file, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['sender_id', 'successful'])
            for sender_id, successful in track_successful_ids.items():
                writer.writerow([sender_id, successful])

    def _write_success_rate(self, file_path: str, stats: dict, file_suffix: str, key_label: str) -> None:
        """Write the success rate to a CSV file.
        Explain steps:
        1. Calculate the success rate per file_name or original_story_id.
        2. Write the success rate to a CSV file.
        args:
            file_path: str
            stats: dict
            file_suffix: str
            key_label: str
        """
        success_rate_file = file_path.replace('.csv', file_suffix)
        total_successful_tests, total_tests = 0, 0

        with open(success_rate_file, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([key_label, 'Success Rate', 'Successful Tests', 'Total Tests'])

            for key, stat in stats.items():
                total, successful = stat['total'], stat['successful']
                rate = (successful / total * 100) if total > 0 else 0
                writer.writerow([key, f"{rate:.2f}%", successful, total])

                total_successful_tests += successful
                total_tests += total

            average_rate = (total_successful_tests / total_tests * 100) if total_tests > 0 else 0
            writer.writerow(['Average/Total', f"{average_rate:.2f}%", total_successful_tests, total_tests])
        
class RasaCorePredictor(CorePredictor):
    def __init__(self, rasa_webhook_url: str, media_responses_url: str, semaphore_limit: int = 10):
        self.RASA_WEBHOOK_URL = rasa_webhook_url
        self.MEDIA_RESPONSES_URL = media_responses_url
        self.utter_responses = self.load_utters()
        self.semaphore = asyncio.Semaphore(semaphore_limit)
        self.cache = {}
    
    def load_utters(self):
        """
        Load the utterances from a YAML file via a GET request.
        returns:
            Dict[str, List[Dict[str, str]]]
        """
        headers = {'Accept': 'application/yaml'}
        try:
            # Make a GET request to fetch the YAML file
            response = requests.get(self.MEDIA_RESPONSES_URL, headers=headers, timeout=15)
            response.raise_for_status()  # Raise an error for HTTP status codes 4xx/5xx

            # Parse the YAML content
            yaml_content = yaml.safe_load(response.text)
            utter_responses = yaml_content.get("responses", {})
            return utter_responses

        except requests.exceptions.RequestException as e:
            print(f"HTTP error occurred: {e}")
            return {}
        except yaml.YAMLError as e:
            print(f"Error parsing YAML content: {e}")
            return {}
    
    async def predict(
        self, session: aiohttp.ClientSession, sender_id: str, message: str,
        message_sequence: Tuple[str], custom_data: Dict[str, Any],
        story_id: str, file_name: str, max_retries: int = 3
    ) -> List[Dict[str, Any]]:
        """Predict the response from the Rasa Core API.
        args:
            session: aiohttp.ClientSession
            sender_id: str
            message: str
            message_sequence: Tuple[str]
            custom_data: Dict[str, Any]
            story_id: str
            file_name: str
            max_retries: int
        returns:
            List[Dict[str, Any]]
        """
        if message_sequence:
            cache_key = f"{str(message_sequence)}"
            if cache_key in self.cache:
                return self.cache[cache_key]

        async with self.semaphore:
            payload = {"sender": sender_id, "message": message, "customData": custom_data}

            for attempt in range(max_retries):
                try:
                    async with session.post(self.RASA_WEBHOOK_URL, json=payload) as response:
                        response_data = await response.json()
                        if message_sequence:
                            self.cache[cache_key] = response_data
                        return response_data
                except Exception as e:
                    logger.error(f"Error parsing message (attempt {attempt + 1}/{max_retries}): {e}")
                    await asyncio.sleep(1)
        return []

    @staticmethod
    async def parse(response_data: List[Dict[str, Any]]) -> Tuple[List[str], List[List[Dict[str, str]]]]:
        """Parse the response data from the Rasa Core API.
        args:
            response_data: List[Dict[str, Any]]
        returns:
            Tuple[List[str], List[List[Dict[str, str]]]]
        """
        parsed_response = []
        parsed_buttons = []
        for json_response in response_data:
            template_name = json_response.get("custom", {}).get("template_name")
            text = json_response.get("text")
            buttons = json_response.get("buttons", [])
            if template_name:
                parsed_response.append(template_name)
                parsed_buttons.append(buttons)
            else:
                parsed_response.append(text)
                parsed_buttons.append(buttons)

        parsed_response = [response for response in parsed_response if response]
        parsed_buttons = [button for sublist in parsed_buttons if sublist for button in sublist]

        return parsed_response, parsed_buttons

class ConversationProcessor:
    @staticmethod
    def generate_variations(conversation):
        """Generate all possible variations of a conversation based on the input fields.
        args:
            conversation: List[Dict]
        returns:
            List[Tuple[str]]
        """
        message_lists = [
            step.get("messages") or step.get("input")
            for step in conversation
            if step.get("messages") or step.get("input")
        ]
        return list(product(*message_lists))

    @staticmethod
    def safe_get(conversation, step_idx, key, variation_idx, default=None):
        """
        Safely retrieves an element from a list in a dictionary, handling None and index errors.
        
        :param conversation: The conversation dictionary.
        :param step_idx: The index of the step in the conversation.
        :param key: The key to retrieve the list.
        :param variation_idx: The primary index to retrieve.
        :param default: The default value to return if the index is invalid or not found.
        :return: The retrieved element or the default value.
        """
        values = conversation[step_idx].get(key, [None])
        if values is None or not isinstance(values, list):
            return default
        
        # Try to get the value at variation_idx first
        if variation_idx < len(values):
            value = values[variation_idx]
            if value is not None:
                return value

        # If variation_idx is not valid, try to get the value at index 0
        if len(values) > 0:
            value = values[0]
            if value is not None:
                return value

        # Fallback to default if neither index has a valid value
        return default
    
    @staticmethod
    def get_utter_text(responses, predictor) -> List[str]:
        """Get the utterance text from the domain if the response is an utterance.
        args:
            responses: List[str]
            predictor: RasaCorePredictor
        returns:
            List[str]
        """
        try:
            # create deep copy of responses
            copy_responses = copy.deepcopy(responses)
            for idx, response in enumerate(copy_responses):
                if response.startswith("utter_"):
                    copy_responses[idx] = [response.get("text") for response in predictor.utter_responses.get(response)][0]
                else:
                    copy_responses[idx] = response
        except Exception as e:
            logger.error(f"Error getting utter text: {e}")
            logger.error(f"Error for Responses: {responses}")
        
        return copy_responses

    @staticmethod
    async def process_conversation(
        session: aiohttp.ClientSession, story: Dict,
        file_path: str, file_name: str, core_predictor: RasaCorePredictor,
        custom_data: Dict[str, Any], local_mode: bool = False
    ) -> List[Dict]:
        """Process a conversation and generate variations for each step.
        args:
            session: aiohttp.ClientSession
            story: Dict
            file_path: str
            file_name: str
            core_predictor: RasaCorePredictor
            custom_data: Dict[str, Any]
            local_mode: bool
        returns:
            List[Dict]
        """
        variations = []
        story_id = story["story_id"]
        original_story_id = story.get("original_story_id", story_id)
        expected_failure = story.get("expected_failure", False)
        conversation = story["conversation"]
        total_variations = ConversationProcessor.generate_variations(conversation)

        for variation_idx, messages in enumerate(total_variations):
            message_sequence = []
            current_time_date = datetime.now(pytz.timezone("Europe/Amsterdam")).strftime("%d-%m-%Y_%H:%M:%S")
            current_turn = 0

            for step_idx, message in enumerate(messages):
                expected_response = (
                    conversation[step_idx].get("expected_responses") or
                    conversation[step_idx].get("output", {}).get("responses", [])
                )

                expected_buttons = [
                    button
                    for response in expected_response
                    if "buttons" in response
                    for sublist in response["buttons"]
                    for button in (sublist if isinstance(sublist, list) else [sublist])
                ]

                expected_response = [
                    response.get("utter_name") or response.get("text", response) if isinstance(response, dict) else response
                    for response in expected_response
                ]

                if any(isinstance(i, list) for i in expected_response):
                    expected_response = [item for sublist in expected_response for item in sublist]

                predicted_message = (
                    ConversationProcessor.safe_get(conversation, step_idx, "messages_predicted", variation_idx, 0) or
                    ConversationProcessor.safe_get(conversation, step_idx, "input_predicted", variation_idx, 0)
                )

                confidence = (
                    ConversationProcessor.safe_get(conversation, step_idx, "messages_predicted_confidences", variation_idx, 0) or
                    ConversationProcessor.safe_get(conversation, step_idx, "input_predicted_confidences", variation_idx, 0)
                )

                message_sequence.append(str(predicted_message))

                sender_id = f"end2end_{story_id}_variation_{variation_idx}"
                sender_id_time = f"{sender_id}_{current_time_date}"

                #TODO: temp hack for role. pass message instead of predicted_message. role doesnt work when passed to core.
                try:
                    if "role" in predicted_message:
                        response_data = await core_predictor.predict(
                            session,
                            sender_id_time,
                            str(message),
                            tuple(message_sequence),
                            custom_data=custom_data,
                            story_id=story_id,
                            file_name=file_name,
                        )
                    else:
                        response_data = await core_predictor.predict(
                            session,
                            sender_id_time,
                            str(predicted_message),
                            tuple(message_sequence),
                            custom_data=custom_data,
                            story_id=story_id,
                            file_name=file_name,
                        )
                except Exception as e:
                    logger.error(f"Error predicting message: {e}")
                    logger.error(f"Error for Story ID: {story_id}")
                    continue
                
                parsed_response, parsed_buttons = await core_predictor.parse(response_data)

                # Check if the expected response is equal to the parsed response
                original_response_equal = expected_response == parsed_response
                original_expected_response = expected_response
                original_predicted_response = parsed_response

                # Get the utterance text from the domain if the response is an utterance
                expected_response = ConversationProcessor.get_utter_text(expected_response, core_predictor)
                parsed_response = ConversationProcessor.get_utter_text(parsed_response, core_predictor)
                
                response_success = expected_response == parsed_response
                buttons_success = expected_buttons == parsed_buttons

                variation = {
                    "sender_id": sender_id,
                    "sender_id_time": sender_id_time,
                    "story_id": story_id,
                    "original_story_id": original_story_id,
                    "turn": current_turn,
                    "step": step_idx,
                    "message": message,
                    "predicted_intent": predicted_message,
                    "original_expected_response": original_expected_response,
                    "original_predicted_response": original_predicted_response,
                    "predicted_response": parsed_response,
                    "expected_response": expected_response,
                    "original_response_equal": original_response_equal,
                    "expected_buttons": expected_buttons,
                    "predicted_buttons": parsed_buttons,
                    "confidence": confidence,
                    "expected_failure": expected_failure,
                    "file_name": file_name,
                    "response_success": response_success,
                    "buttons_success": buttons_success,
                    "timestamp": datetime.now(pytz.timezone('Europe/Amsterdam')).strftime('%d-%m-%Y %H:%M:%S')
                }
                variations.append(variation)
                current_turn += 1
            
            if local_mode:
                await core_predictor.predict(
                        session, sender_id, "/opnieuw", None,
                        custom_data=custom_data, story_id=story_id, file_name=file_name
                )

        return variations

class DirectoryProcessor:
    def __init__(self, file_reader: FileReader, core_predictor: RasaCorePredictor, csv_writer: CsvFileWriter, output_file: str = "output.csv"):
        self.file_reader = file_reader
        self.core_predictor = core_predictor
        self.csv_writer = csv_writer
        self.custom_data = {
            "guid": "acd3da2b-4906-45da-8373-96e632c899de",
            "livechat": "gesloten",
            "username": "end2end",
            "municipality": "Witterland",
            "feature_livechat": "gesloten"
        }
        self.results_path = "end2end/results/"
        self.output_file = output_file

    async def process_directories(self, directories: List[str], story_names: List[str], filenames: List[str]) -> None:
        """Process the directories and write the results to a CSV file.
        args:
            directories: List[str]
            story_names: List[str]
            filenames: List[str]
        """
        tasks = []
        for directory in directories:
            for root, _, files in os.walk(directory):
                logger.info(f"Processing directory: {root}")
                for file in files:
                    if (file.endswith('.yaml') or file.endswith('.yml')) and (not filenames or file in filenames):
                        file_path = os.path.join(root, file)
                        data = self.file_reader.read(file_path)
                        if self.should_process_file(data, story_names):
                            tasks.append(self.process_stories(data, file_path, file))
        
        results = await asyncio.gather(*tasks)
        all_variations = [variation for result in results for variation in result]

        os.makedirs(self.results_path, exist_ok=True)
        self.csv_writer.write(os.path.join(self.results_path, self.output_file), all_variations)

    @staticmethod
    def should_process_file(data: List[Dict], story_names: List[str]) -> bool:
        """Check if the file should be processed based on the story names.
        args:
            data: List[Dict]
            story_names: List[str]
        returns:
            bool
        """
        if not story_names:
            return True
        return any(story.get('story', '') in story_names for story in data)


    def split_chains(self, story: Dict) -> List[Dict]:
        """Split chains in a story into separate stories.
        Args:
            story: Dict
        Returns:
            List[Dict]
        """

        # Create a base story with an empty conversation list
        base_story = copy.deepcopy(story)
        base_story['conversation'] = []
        split_stories = []

        def process_chain(current_story, remaining_chain):
            if not remaining_chain:
                split_stories.append(current_story)
                return

            step = remaining_chain[0]
            current_story['conversation'].append(step)

            if 'chain' in step:
                for chain_step in step['chain']:
                    new_story = copy.deepcopy(current_story)
                    # Ensure the new conversation list is deep copied
                    new_story['conversation'] = copy.deepcopy(new_story['conversation'])
                    new_story['conversation'].append(chain_step)
                    process_chain(new_story, remaining_chain[1:])
            else:
                process_chain(current_story, remaining_chain[1:])

        # Start processing with the initial conversation chain
        process_chain(base_story, [story['conversation']])

        # Rename and add metadata to each split story
        for idx, split_story in enumerate(split_stories):
            original_name = split_story.get('story_name', '')
            split_story['story_name'] = f"{original_name} - VARIANT {idx + 1}"
            split_story['original_story_id'] = story['story_id']
            split_story['story_id'] = f"{story['story_id']}_variant_{idx + 1}"

        return split_stories

    async def process_stories(self, data: List[Dict], file_path: str, file_name: str) -> List[Dict]:
        """Process the stories and generate variations for each story.
        args:
            data: List[Dict]
            file_path: str
            file_name: str
        returns:
            List[Dict]
        """
        tasks = []
        async with aiohttp.ClientSession() as session:
            for story in data:
                split_stories = self.split_chains(story)
                for split_story in split_stories:
                    tasks.append(ConversationProcessor.process_conversation(
                        session, split_story, file_path, file_name, self.core_predictor, self.custom_data, local_mode=False
                    ))
            
            results = await asyncio.gather(*tasks)
        return [variation for result in results for variation in result]
    
async def main(directories: List[str], story_names: List[str], filenames: List[str], rasa_url: str, environment: str) -> None:
    start_time = datetime.now()
    if not directories:
        logger.info("No directories provided... Using default directories.")
        temp_simple_stories_dir = "parsed_simple_stories/functional_tests"
        logger.info(f"Using default directory: {temp_simple_stories_dir}")
        directories = [temp_simple_stories_dir]

    file_reader = YamlFileReader()
    csv_writer = CsvFileWriter()
    rasa_webhook_url = f"{rasa_url}/webhooks/test/webhook"
    media_container_url = os.getenv("MEDIA_CONTAINER_URL", "http://media:80")
    repository_name = os.getenv("REPOSITORY_NAME", "gem")
    responses_path = os.getenv("RESPONSES_PATH", "domain/responses.yml")
    media_responses_url = f"{media_container_url}/{repository_name}/{environment}/{responses_path}"
    core_predictor = RasaCorePredictor(rasa_webhook_url, media_responses_url)
    directory_processor = DirectoryProcessor(file_reader, core_predictor, csv_writer)
    await directory_processor.process_directories(directories, story_names, filenames)
    
    end_time = datetime.now()
    duration = end_time - start_time
    logger.info(f"Total duration in minutes: {duration.total_seconds() / 60}")

    # return analysis_results

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process directories for specific stories and filenames.")
    parser.add_argument("--directories", nargs="+", help="Directories to process.")
    parser.add_argument("--story-names", nargs="+", help="Specific story names to process.")
    parser.add_argument("--filenames", nargs="+", help="Specific filenames to process.")
    parser.add_argument("--rasa_url", help="Rasa NLU URL.", default="http://rasa-script:5005")
    parser.add_argument("--environment", help="Environment name.", default="pre")

    args = parser.parse_args()
    asyncio.run(main(args.directories, args.story_names or [], args.filenames or [], args.rasa_url, args.environment))