import os
import yaml
import asyncio
import aiohttp
import argparse
from abc import ABC, abstractmethod
from typing import List, Dict, Any, Tuple
from collections import OrderedDict
import datetime
import asyncio
import shutil
import os
from typing import List
import logging

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

async def nlu_parse(
        sem: asyncio.Semaphore,
        session: aiohttp.ClientSession,
        rasa_url: str,
        message: str,
        max_retries: int = 3
        ) -> Tuple[str, float]:
    
    """Send a message to Rasa NLU and get the response
    param sem: The semaphore to limit concurrent requests
    param session: The aiohttp session
    param message: The message to send to Rasa NLU
    param max_retries: The maximum number of retries
    return: The intent and confidence
    """
    async with sem:
        for attempt in range(max_retries):
            try:
                start_timer = datetime.datetime.now()
                async with session.post(
                    f"{rasa_url}/model/parse",
                    json={"text": message},
                    timeout=aiohttp.ClientTimeout(total=1000)
                ) as response:
                    response_data = await response.json()

                    # Extract entities and handle roles in your desired format
                    entities = []
                    for entity in response_data["entities"]:
                        if "role" in entity:
                            # Append entity and role together
                            entities.append(f'"{entity["entity"]}": "{entity["value"]}", "role": "{entity["role"]}"')
                        else:
                            # Normal entity with no role
                            entities.append(f'"{entity["entity"]}": "{entity["value"]}"')

                    # Join entities into the correct format
                    join_entities = '{' + ", ".join(entities) + '}'

                    # Format the result with intent and entities
                    result = f'/{response_data["intent"]["name"]}{join_entities}'
                    confidence = response_data["intent"]["confidence"]

                    end_timer = datetime.datetime.now()
                    duration = (end_timer - start_timer).total_seconds()

                    return result, confidence
            except Exception as e:
                logger.error(f"Error parsing message (attempt {attempt + 1}/{max_retries}): {e}")
                logger.error(f"Message: {message}")
                await asyncio.sleep(1)  # Add a small delay before retrying

        # If all attempts fail, return None
        return None, None

# Abstract Reader class
class FileReader(ABC):
    @abstractmethod
    def read(self, file_path: str):
        pass

# YamlFileReader class that implements FileReader
class YamlFileReader(FileReader):
    def read(self, file_path: str) -> Any:
        with open(file_path, 'r') as file:
            return yaml.safe_load(file)
        
# Abstract IntentPredictor class
class IntentPredictor(ABC):
    @abstractmethod
    async def predict(self, message: str) -> Tuple[str, float]:
        pass

# RasaIntentPredictor class using the nlu_parse function
class RasaIntentPredictor(IntentPredictor):
    def __init__(self, sem: asyncio.Semaphore, session: aiohttp.ClientSession, rasa_url: str):
        self.sem = sem
        self.session = session
        self.rasa_url = rasa_url

    async def predict(self, message: str) -> Tuple[str, float]:
        return await nlu_parse(self.sem, self.session, self.rasa_url, message)

class DirectoryProcessor:
    def __init__(self, file_reader: FileReader, intent_predictor: IntentPredictor):
        self.file_reader = file_reader
        self.intent_predictor = intent_predictor

    async def process_directories(self, directories: List[str], story_names: List[str], filenames: List[str]):
        tasks = []
        for directory in directories:
            for root, _, files in os.walk(directory):
                for file in files:
                    if (file.endswith('.yaml') or file.endswith('.yml')) and (not filenames or file in filenames):
                        file_path = os.path.join(root, file)
                        data = self.file_reader.read(file_path)
                        if self.should_process_file(data, story_names):
                            tasks.append(self.process_stories(data, file_path))
        
        await asyncio.gather(*tasks)

    def should_process_file(self, data: List[Dict], story_names: List[str]) -> bool:
        if not story_names:
            return True
        for story in data:
            if story.get('story', '') in story_names:
                return True
        return False

    async def process_stories(self, data: List[Dict], file_path: str):
        tasks = []
        if data:
            for story in data:
                tasks.append(self.process_conversation(story.get('conversation')))
                # for conversation in story.get('conversation', []):
                    # tasks.append(self.process_conversation(conversation))
        
        await asyncio.gather(*tasks)
        # if file already exists, it will be overwritten
        file_name, file_ext = os.path.splitext(file_path)
        new_file_path = f"{file_name}{file_ext}"

        with open(new_file_path, 'w') as new_file:
            yaml.dump(data, new_file, sort_keys=False,allow_unicode=True)
            
    async def process_conversation(self, conversation: Dict):
        ordered_conversation = OrderedDict()
        confidences = []

        tasks = []
        for key, value in conversation.items():
            ordered_conversation[key] = value
            if key in ('messages', 'input'):
                tasks.append((key, value))

        results = await asyncio.gather(*[self.predict_intents(key, value) for key, value in tasks])
        
        for (key, _), predicted_intents in zip(tasks, results):
            predicted_key = f'{key}_predicted'
            ordered_conversation[predicted_key] = [intent for intent, confidence in predicted_intents]
            confidences.extend([confidence for _, confidence in predicted_intents])
            ordered_conversation[f'{predicted_key}_confidences'] = confidences

        for key, value in conversation.items():
            if key == 'chain':
                for chain_conversation in value:
                    await self.process_conversation(chain_conversation)

        conversation.clear()
        conversation.update(self.order_conversation(ordered_conversation))

    async def predict_intents(self, key: str, messages: List[str]):
        return await asyncio.gather(*[self.intent_predictor.predict(msg) for msg in messages])

    def order_conversation(self, conversation: OrderedDict) -> OrderedDict:
        ordered_conversation = OrderedDict()
        keys_order = ['input', 'input_predicted', 'input_predicted_confidences', 'messages', 'messages_predicted', 'messages_predicted_confidences']
        
        for key in keys_order:
            if key in conversation:
                ordered_conversation[key] = conversation[key]

        for key, value in conversation.items():
            if key not in ordered_conversation:
                ordered_conversation[key] = value
        
        return ordered_conversation

# Function to execute the processing
async def main(directories: List[str], story_names: List[str], filenames: List[str], rasa_url: str):
    # start_time
    # TODO: use os.getcwd() to get the current working directory and write the outputs to this directory
    # os.getcwd()

    start_time = datetime.datetime.now()
    if not directories:
        logger.info("No directories provided... Using default directories.")
        simple_stories_path = "data/simple-stories/functional_tests"
        logger.info(f"Using default directories: {simple_stories_path}")
        
        # Define the specific temporary directories
        temp_simple_stories_dir = "parsed_simple_stories"
        
        # Ensure the temporary directories exist
        os.makedirs(temp_simple_stories_dir, exist_ok=True)
        
        # Define the paths for the copied directories
        temp_simple_stories_path = os.path.join(temp_simple_stories_dir, "functional_tests")
        
        # Copy simple_stories_path to the temp directory
        if os.path.exists(temp_simple_stories_path):
            shutil.rmtree(temp_simple_stories_path)
        shutil.copytree(simple_stories_path, temp_simple_stories_path)
        logger.info(f"Copied {simple_stories_path} to {temp_simple_stories_path}")

        directories = [temp_simple_stories_path]
        
    yaml_reader = YamlFileReader()
    async with aiohttp.ClientSession() as session:
        sem = asyncio.Semaphore(10)  # Limit concurrent requests
        intent_predictor = RasaIntentPredictor(sem, session, rasa_url)
        processor = DirectoryProcessor(yaml_reader, intent_predictor)
        await processor.process_directories(directories, story_names, filenames)
    # end_time
    end_time = datetime.datetime.now()
    # duration in minutes
    duration = (end_time - start_time).total_seconds() / 60
    print(f"Duration: {duration} minutes")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process directories for specific stories and filenames.")
    parser.add_argument("--directories", nargs="+", help="Directories to process.")
    parser.add_argument("--story-names", nargs="+", help="Specific story names to process.")
    parser.add_argument("--filenames", nargs="+", help="Specific filenames to process.")
    parser.add_argument("--rasa_url", help="Rasa NLU URL.", default="http://rasa-script:5005")

    args = parser.parse_args()
    asyncio.run(main(args.directories, args.story_names or [], args.filenames or [], args.rasa_url))