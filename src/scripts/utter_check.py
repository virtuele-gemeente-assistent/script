import os
from ruamel.yaml import YAML 

def load_yaml_file(file_path):
    print("file_path: ", file_path)
    yaml = YAML() 
    with open(file_path, 'r', encoding='utf-8') as file:
        return yaml.load(file) 

def get_utter_names_from_tests(test_folder):
    utter_names = set()
    for root, dirs, files in os.walk(test_folder):
        for file in files:
            if file.endswith('.yml') or file.endswith('.yaml'):
                test_data = load_yaml_file(os.path.join(root, file))
                for story in test_data:
                    for step in story['conversation']:
                        for response in step['expected_responses']:
                            utter_names.add(response['utter_name'])
    return utter_names

def find_missing_utters(test_folders, responses_file):
    responses_data = load_yaml_file(responses_file)
    responses_names = set(responses_data['responses'].keys())

    test_utter_names = set()
    for folder in test_folders:
        test_utter_names |= get_utter_names_from_tests(folder)

    missing_utters = test_utter_names - responses_names
    return missing_utters

test_folders = [
    '/var/data/gem/end2end/test_data/functional_tests/',
    '/var/data/gem/end2end/test_data/technical_tests/'
]
responses_file = '/var/data/gem/rasa/domain/responses.yml'

missing_utters = find_missing_utters(test_folders, responses_file)

print("Utterances found in the test files but not in responses.yml:")
for utter in sorted(missing_utters):
    print(utter)
