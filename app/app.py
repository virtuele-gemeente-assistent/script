import asyncio
import json as json_module
import logging
import os
import shutil
import sys
import urllib.parse
from datetime import datetime
from typing import Any, Dict, List, Optional

import aiohttp
import pytz
from sanic import Request, Sanic
from sanic.response import HTTPResponse, file, html, json, text

# Sentry setup
sentry_dsn = os.getenv("SCRIPT_SENTRY_DSN", None)
if sentry_dsn:
    import sentry_sdk
    from sentry_sdk.integrations.redis import RedisIntegration
    from sentry_sdk.integrations.sanic import SanicIntegration

    sentry_sdk.init(
        dsn=sentry_dsn, integrations=[SanicIntegration(), RedisIntegration()]
    )


# Logger class to encapsulate logging functionality
class Logger:
    def __init__(self, name: str = __name__):
        self._logger = logging.getLogger(name)
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            handlers=[logging.StreamHandler(sys.stdout)],
        )

    def info(self, message: str) -> None:
        self._logger.info(message)

    def error(self, message: str) -> None:
        self._logger.error(message)

    def warning(self, message: str) -> None:
        self._logger.warning(message)


# Manages the collection of jobs and their respective states and executions
class ScriptManager:
    def __init__(self, logger: Logger):
        self.jobs: Dict[str, Dict[str, Dict[str, Any]]] = {}
        self.logger = logger

    async def run_script(self, script: "Script", environment: str) -> None:
        etag = script.job_name
        end_time = None
        try:
            process = await script.execute()
            etag_dict = self.jobs.setdefault(environment, {}).setdefault(
                etag, {"scripts": {}}
            )
            etag_dict["scripts"][script.script_name]["process"] = process

            if process:
                await script.capture_output(process, self.logger, environment)
                status = "completed" if process.returncode == 0 else "failed"
                if status == "completed":
                    amsterdam_tz = pytz.timezone("Europe/Amsterdam")
                    end_time = datetime.now(amsterdam_tz).strftime("%Y-%m-%dT%H:%M:%S")
            else:
                status = "failed"
        except Exception as e:
            status = "failed"
            self.logger.error(
                f"Exception while running script '{script.script_name}' for job '{etag}' in environment '{environment}': {str(e)}"
            )
        finally:
            try:
                # Safely update the status of the script
                job_scripts = (
                    self.jobs.get(environment, {}).get(etag, {}).get("scripts", {})
                )
                if script.script_name in job_scripts:
                    job_scripts[script.script_name]["status"] = status
                    if status == "completed":
                        job_scripts[script.script_name]["end_time"] = end_time
                else:
                    self.logger.warning(
                        f"Script '{script.script_name}' for job '{etag}' not found during status update."
                    )
            except Exception as e:
                self.logger.error(
                    f"Error updating script status for '{script.script_name}': {str(e)}"
                )
            self.logger.info(
                f"Script '{script.script_name}' for job '{etag}' in environment '{environment}' final status: {status}"
            )

    def start_script(self, script: "Script", environment: str, start_time=None) -> None:
        etag = script.job_name
        etag_dict = self.jobs.setdefault(environment, {}).setdefault(
            etag, {"scripts": {}}
        )

        script_entry = etag_dict["scripts"].get(script.script_name, {})
        script_entry.update(
            {
                "status": "running",
                "etag": etag,
                "args": script.script_args,
                "log": [],
            }
        )
        if start_time:
            script_entry["start_time"] = start_time

        etag_dict["scripts"][script.script_name] = script_entry

        # Start asyncio task for script execution related to this specific job and environment
        asyncio.create_task(self.run_script(script, environment))

    def get_job_status(self, environment: str, etag: str) -> Optional[Dict[str, str]]:
        job = self.jobs.get(environment, {}).get(etag)
        if not job:
            return None
        return {
            script_name: script_info.get("status", "unknown")
            for script_name, script_info in job["scripts"].items()
        }

    def cancel_job(
        self, environment: str, etag: str, script_name: Optional[str] = None
    ) -> bool:
        job = self.jobs.get(environment, {}).get(etag)
        if not job:
            self.logger.warning(
                f"No such job '{etag}' in environment '{environment}' found."
            )
            return False

        if script_name:
            script = job["scripts"].get(script_name)
            if script and script["status"] == "running":
                process = script.get("process")
                if process:
                    try:
                        process.terminate()
                    except Exception as e:
                        self.logger.error(
                            f"Error terminating script '{script_name}': {e}"
                        )
                    script["status"] = "cancelled"
                    self.logger.info(
                        f"Script '{script_name}' for job '{etag}' in environment '{environment}' has been cancelled."
                    )
                    return True
            self.logger.warning(
                f"Failed to cancel script '{script_name}' for job '{etag}' in environment '{environment}' or job is not running."
            )
            return False
        else:
            for script_name, script in job["scripts"].items():
                process = script.get("process")
                if process and script["status"] == "running":
                    try:
                        process.terminate()
                    except Exception as e:
                        self.logger.error(
                            f"Error terminating script '{script_name}': {e}"
                        )
                    script["status"] = "cancelled"
            self.logger.info(
                f"All scripts for job '{etag}' in environment '{environment}' have been cancelled."
            )
            return True

    def cancel_running_scripts_for_environment(self, environment: str) -> None:
        """
        Iterates through all jobs in the given environment and cancels any running scripts.
        """
        env_jobs = self.jobs.get(environment, {})
        for etag in list(env_jobs.keys()):
            self.cancel_job(environment, etag)


# Represents a script to be executed along with its configurations and arguments
class Script:
    script_base_path: str = "/usr/local/src/scripts"

    def __init__(
        self,
        workdir: str,
        job_name: str,
        script_name: str,
        script_args: List[str],
        manager: ScriptManager,
    ):
        self.workdir = workdir
        self.job_name = job_name
        self.script_name = script_name
        self.script_args = script_args
        self.manager = manager

    def absolute_script_path(self) -> str:
        path = os.path.abspath(f"{self.script_base_path}/{self.script_name}")
        return path if path.endswith(".py") else path + ".py"

    async def execute(self) -> Optional[asyncio.subprocess.Process]:
        command = [sys.executable, self.absolute_script_path()] + self.script_args
        if self.validate_paths():
            try:
                return await asyncio.create_subprocess_exec(
                    *command,
                    stdout=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE,
                    cwd=self.workdir,
                )
            except Exception as e:
                self.manager.logger.error(
                    f"Failed to execute script '{self.script_name}': {e}"
                )
        return None

    def validate_paths(self) -> bool:
        if not os.path.exists(self.workdir):
            self.manager.logger.error(f"Workdir does not exist: {self.workdir}")
            return False
        if not os.path.isfile(self.absolute_script_path()):
            self.manager.logger.error(
                f"Script file not found: {self.absolute_script_path()}"
            )
            return False
        return True

    async def capture_output(
        self, process: asyncio.subprocess.Process, logger: Logger, environment: str
    ) -> None:
        if not process:
            logger.error(f"Process for script '{self.script_name}' is not initialized.")
            return

        while True:
            stdout_line = await process.stdout.readline()
            stderr_line = await process.stderr.readline()

            if stdout_line:
                decoded_stdout = stdout_line.decode().strip()
                self.update_job_log(decoded_stdout, logger, environment, "stdout")
                logger.info(f"Job '{self.job_name}' stdout: {decoded_stdout}")

            if stderr_line:
                decoded_stderr = stderr_line.decode().strip()
                self.update_job_log(decoded_stderr, logger, environment, "stderr")
                if "INFO:" in decoded_stderr or "DEBUG:" in decoded_stderr:
                    logger.info(f"Job '{self.job_name}' stderr (log): {decoded_stderr}")
                elif "ERROR" in decoded_stderr or "CRITICAL" in decoded_stderr:
                    logger.error(f"Job '{self.job_name}' stderr: {decoded_stderr}")
                else:
                    logger.warning(f"Job '{self.job_name}' stderr: {decoded_stderr}")

            if process.stdout.at_eof() and process.stderr.at_eof():
                break

        await process.wait()

    def update_job_log(
        self, decoded_line: str, logger: Logger, environment: str, stream_type: str
    ) -> None:
        script_log = self.manager.jobs[environment][self.job_name]["scripts"][
            self.script_name
        ]["log"]
        script_log.append(decoded_line)

        log_method = logger.info if stream_type == "stdout" else logger.error
        log_method(f"Job '{self.job_name}' {stream_type}: {decoded_line}")


# Handles downloading and storing files
class FileDownloader:
    @staticmethod
    async def download(
        base_url: str, workdir: str, overwrite: bool, logger: Logger, etag: str
    ) -> None:
        final_workdir = f"{workdir}/{etag}"

        if not overwrite and os.path.exists(final_workdir):
            logger.info(
                f"Directory {final_workdir} already exists and is not empty. Skipping download."
            )
            return

        os.makedirs(final_workdir, exist_ok=True)
        async with aiohttp.ClientSession() as session:
            await FileDownloader.download_recursive(
                base_url, final_workdir, session, logger
            )

    @staticmethod
    async def get_etag(url: str, session: aiohttp.ClientSession) -> str:
        async with session.head(url) as response:
            return response.headers.get("ETag", "")

    @staticmethod
    async def download_recursive(
        base_url: str, current_dir: str, session: aiohttp.ClientSession, logger: Logger
    ) -> None:
        async with session.get(base_url) as response:
            if response.status == 200:
                items = await response.json()

                download_tasks = []

                for item in items:
                    if item["type"] == "file":
                        download_tasks.append(
                            FileDownloader.download_single_file(
                                item["name"], base_url, current_dir, session, logger
                            )
                        )
                    elif item["type"] == "directory":
                        new_dir = os.path.join(current_dir, item["name"])
                        os.makedirs(new_dir, exist_ok=True)
                        download_tasks.append(
                            FileDownloader.download_recursive(
                                f"{base_url}/{item['name']}", new_dir, session, logger
                            )
                        )

                await asyncio.gather(*download_tasks)
            else:
                logger.error(f"Failed to fetch directory listing at {base_url}.")

    @staticmethod
    async def download_single_file(
        filename: str,
        base_url: str,
        workdir: str,
        session: aiohttp.ClientSession,
        logger: Logger,
    ) -> None:
        file_url = f"{base_url}/{filename}"
        async with session.get(file_url) as file_response:
            if file_response.status == 200:
                file_path = os.path.join(workdir, filename)
                with open(file_path, "wb") as file:
                    file.write(await file_response.read())
                logger.info(f"Downloaded file: {filename}")
            else:
                logger.error(f"Failed to download file: {filename} from {file_url}")


# Manages navigation through the etag mapje
class ResultFileManager:
    def __init__(self, base_workdir: str):
        self.base_workdir = base_workdir

    def list_directory_contents(self, etag_folder: str) -> List[Dict[str, str]]:
        etag_folder_path = os.path.join(self.base_workdir, etag_folder)

        if not os.path.exists(etag_folder_path):
            return [{"error": "Path does not exist."}]

        listings = []
        for entry in os.listdir(etag_folder_path):
            path = os.path.join(etag_folder_path, entry)
            entry_type = "directory" if os.path.isdir(path) else "file"
            listings.append({"name": entry, "type": entry_type})

        return listings


# Initialize app components
app = Sanic("ScriptManager")
logger = Logger()
manager = ScriptManager(logger)
result_file_manager = ResultFileManager(os.getenv("BASE_WORKDIR", "/tmp"))


async def prune_old_versions(environment: str, max_versions: int = 5) -> None:
    """Remove older etag versions, retaining only the latest `max_versions` based on time."""
    etag_dict = manager.jobs.get(environment, {})

    etags_with_timestamps = []
    for etag, job_data in etag_dict.items():
        scripts = job_data.get("scripts", {})
        if scripts:
            first_script = next(iter(scripts.values()), {})
            start_time = first_script.get("start_time")
            if start_time:
                try:
                    start_time = datetime.strptime(start_time, "%Y-%m-%dT%H:%M:%S")
                    etags_with_timestamps.append((etag, start_time))
                except ValueError as e:
                    logger.error(f"Invalid date format for ETag '{etag}': {e}")

    # Sort by start time, latest first
    etags_with_timestamps.sort(key=lambda x: x[1], reverse=True)
    
    # Prune old etags if they exceed max_versions
    if len(etags_with_timestamps) > max_versions:
        old_etags = etags_with_timestamps[max_versions:]
        base_dir = result_file_manager.base_workdir

        for etag, _ in old_etags:
            # Remove from the job dictionary
            del etag_dict[etag]

            # Remove the etag's directory from the filesystem
            etag_dir = os.path.join(base_dir, environment, etag)
            if os.path.exists(etag_dir):
                shutil.rmtree(etag_dir)
                logger.info(f"Deleted old etag directory '{etag_dir}'.")


@app.route("/", methods=["GET"])
async def list_jobs(request: Request) -> HTTPResponse:
    """
    List all jobs in a readable HTML table format.

    JSON Structure:
    {
        "<environment>": {
            "<etag>": {
                "scripts": {
                    "<script_name>": {
                        "status": "<status_value>",
                        "etag": "<etag_value>",
                        "start_time": "<start_time_value>",
                        "end_time": "<end_time_value>",
                        "log": ["<log_line_1>", "<log_line_2>", ...]
                    }
                }
            }
        }
    }
    """
    logger.info("Starting to build the HTML table from manager.jobs data.")
    gitlab_commit_url = "https://gitlab.com/virtuele-gemeente-assistent/gem/-/tree"
    table_html = """
    <html>
        <head>
            <title>Jobs List</title>
            <style>
                table {
                    border-collapse: collapse;
                    width: 100%;
                }
                th, td {
                    text-align: left;
                    padding: 8px;
                    border: 1px solid #ddd;
                }
                th {
                    background-color: #f2f2f2;
                }
                tr:hover {
                    background-color: #f5f5f5;
                }
                details {
                    cursor: pointer;
                }
                details summary {
                    font-weight: bold;
                    margin-bottom: 5px;
                }
                .log-cell ul, .results-cell ul {
                    margin: 0;
                    padding-left: 1.2em;
                }
            </style>
        </head>
        <body>
            <h1>Jobs List</h1>
            <table>
                <thead>
                    <tr>
                        <th>Environment</th>
                        <th>Job ETag</th>
                        <th>Script Name</th>
                        <th>Status</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Log</th>
                        <th>Results</th>
                    </tr>
                </thead>
                <tbody>
    """

    for environment, env_data in manager.jobs.items():
        logger.info(f"Processing environment: {environment}")
        for etag, data in env_data.items():
            logger.info(f"Found ETag for job: {etag}")

            # Build the results dropdown using the full (absolute) path
            job_results_html = ""
            job_results_dir = os.path.abspath(
                os.path.join(result_file_manager.base_workdir, etag, "end2end/results")
            )
            logger.info(f"Current Working Directory: {os.getcwd()}")
            logger.info(f"Checking for job results directory: {job_results_dir}")
            if os.path.isdir(job_results_dir):
                result_files = [
                    f
                    for f in os.listdir(job_results_dir)
                    if os.path.isfile(os.path.join(job_results_dir, f))
                ]
                logger.info(f"Files found in end2end: {result_files}")
                if result_files:
                    job_results_html = (
                        "<details>"
                        "<summary>Show results</summary>"
                        "<ul>"
                        + "".join(
                            f'<li>{file} - <a href="download/{etag}/end2end/results/{urllib.parse.quote(file)}">Download</a></li>'
                            for file in result_files
                        )
                        + "</ul>"
                        "</details>"
                    )

            for script_name, script_data in data.get("scripts", {}).items():
                log_list = script_data.get("log", [])
                if log_list:
                    log_html = (
                        "<details>"
                        "<summary>Show logs</summary>"
                        "<ul>"
                        + "".join([f"<li>{log_line}</li>" for log_line in log_list])
                        + "</ul>"
                        "</details>"
                    )
                else:
                    log_html = ""

                table_html += f"""
                    <tr>
                        <td>{environment}</td>
                        <td><a href="{gitlab_commit_url}/{etag}" target="_blank">{etag}</a></td>
                        <td>{script_name}</td>
                        <td>{script_data.get('status', '')}</td>
                        <td>{script_data.get('start_time', '') or ''}</td>
                        <td>{script_data.get('end_time', '') or ''}</td>
                        <td class="log-cell">{log_html}</td>
                        <td class="results-cell">{job_results_html}</td>
                    </tr>
                """

    table_html += """
                </tbody>
            </table>
        </body>
    </html>
    """
    logger.info("Finished building the HTML table.")
    return html(table_html)


@app.route(
    "/execute/<project_name>/<environment>/<script_name>", methods=["POST", "GET"]
)
async def run_script_with_config(
    request: Request, project_name: str, environment: str, script_name: str
) -> HTTPResponse:
    media_url = os.getenv("MEDIA_URL", "http://media:80")
    full_url = f"{media_url}/{project_name}/{environment}"

    async with aiohttp.ClientSession() as session:
        etag = await FileDownloader.get_etag(full_url, session)

    job_name = etag
    logger.info(f"Script Name: {script_name}, Job Name: {job_name}")

    current_status = manager.get_job_status(environment, job_name)

    if current_status and script_name in current_status:
        logger.info(
            f"Script '{script_name}' for job '{job_name}' exists with status '{current_status[script_name]}'."
        )
        return json(
            {
                "message": f"Script '{script_name}' for job '{job_name}' is already {current_status[script_name]}.",
                "status": current_status[script_name],
                "etag": etag,
            },
            status=200,
        )

    # Cancel any currently running scripts in this environment before starting a new job.
    manager.cancel_running_scripts_for_environment(environment)

    script, workdir, overwrite_if_exist = await prepare_script_execution(
        request, environment, script_name, job_name
    )

    asyncio.create_task(
        FileDownloader.download(full_url, workdir, overwrite_if_exist, logger, etag)
    )

    amsterdam_tz = pytz.timezone("Europe/Amsterdam")
    start_time = datetime.now(amsterdam_tz).strftime("%Y-%m-%dT%H:%M:%S")

    manager.start_script(script, environment, start_time)

    return json(
        {
            "message": f"Script {script_name} started for job {job_name}.",
            "status": "running",
            "etag": etag,
            "start_time": start_time,
        },
        status=200,
    )


async def prepare_script_execution(
    request: Request, environment: str, script_name: str, etag: str
) -> (Script, str, bool):
    base_workdir = os.getenv("BASE_WORKDIR", "/tmp")
    params = await get_request_params(request, base_workdir, environment)

    return (
        Script(
            workdir=f"{base_workdir}/{etag}",
            job_name=etag,
            script_name=script_name,
            script_args=params.script_args,
            manager=manager,
        ),
        base_workdir,
        params.overwrite_if_exist,
    )


class RequestParams:
    def __init__(
        self,
        environment: str,
        base_workdir: str,
        script_args: List[str],
        overwrite_if_exist: bool = False,
    ):
        self.environment = environment
        self.base_workdir = base_workdir
        self.script_args = script_args
        self.overwrite_if_exist = overwrite_if_exist


async def get_request_params(
    request: Request, base_workdir: str, environment: str
) -> RequestParams:
    if request.method == "POST":
        data = request.json
        return RequestParams(
            environment=environment,
            base_workdir=base_workdir,
            script_args=parse_script_args(data.get("args", {}), base_workdir),
            overwrite_if_exist=data.get("overwrite_if_exist", False),
        )
    else:
        script_args = parse_get_args(request, base_workdir)
        return RequestParams(
            environment=environment,
            base_workdir=base_workdir,
            script_args=script_args,
            overwrite_if_exist=request.args.get("overwrite_if_exist", "false").lower()
            == "true",
        )


def create_workdir(base_workdir: str) -> str:
    os.makedirs(base_workdir, exist_ok=True)
    return base_workdir


def parse_script_args(args_data: Dict[str, Any], base_workdir: str) -> List[str]:
    script_args = []
    for key, value in args_data.items():
        if value is not None:
            if isinstance(value, (dict, list)):
                value = json_module.dumps(value)
            if key == "workdir":
                value = os.path.join(base_workdir, value)
            script_args.extend([f"--{key}", str(value)])
    return script_args


def parse_get_args(request: Request, base_workdir: str) -> List[str]:
    script_args = []
    for arg in request.args.getlist("arg", []):
        key, _, value = arg.partition("=")
        if key == "workdir":
            value = os.path.join(base_workdir, value)
        script_args.extend([f"--{key}", value])
    return script_args


@app.route("/execute/<environment>/<job_name>/<script_name>/logs")
async def get_script_logs(
    request: Request, environment: str, job_name: str, script_name: str
) -> HTTPResponse:
    job = manager.jobs.get(environment, {}).get(job_name, {})

    if job:
        script = job["scripts"].get(script_name)
        if script:
            return json({"log": script.get("log", [])}, status=200)

        return text(f"No such script {script_name} in job {job_name}", status=404)

    return text(f"No such job {job_name} in environment {environment}", status=404)


async def download_file(request: Request, etag: str, path: str) -> HTTPResponse:
    decoded_path = urllib.parse.unquote(path)
    file_path = os.path.join(result_file_manager.base_workdir, etag, decoded_path)

    if os.path.isfile(file_path):
        logger.info(f"Serving file download for '{file_path}'.")
        return await file(file_path)
    else:
        logger.error(f"File not found for download '{file_path}'.")
        return text(f"File '{etag}/{path}' not found for downloading.", status=404)


def get_latest_etag(environment_or_etag: str) -> Optional[str]:
    if environment_or_etag in manager.jobs:
        etags = list(manager.jobs[environment_or_etag].keys())
        if etags:
            return etags[-1]
    elif any(environment_or_etag in etags for etags in manager.jobs.values()):
        return environment_or_etag
    logger.error(f"No etags found for '{environment_or_etag}'.")
    return None


@app.route("/files/", methods=["GET"])
async def view_root_environments(request: Request) -> HTTPResponse:
    environments = list(manager.jobs.keys())
    html_content = "<h1>Environments</h1><ul>"
    for environment in environments:
        html_content += f'<li><a href="{environment}/">{environment}</a></li>'
    html_content += "</ul>"
    return text(html_content, headers={"Content-Type": "text/html"})


@app.route("/files/<environment_or_etag>/", methods=["GET"])
async def view_root_files(request: Request, environment_or_etag: str) -> HTTPResponse:
    etag = get_latest_etag(environment_or_etag)
    if etag is None:
        return text(
            f"No valid etag or environment found for '{environment_or_etag}'",
            status=404,
        )
    return await view_files_in_directory(request, etag, path="")


@app.route("/files/<environment_or_etag>/<path:path>", methods=["GET"])
async def view_files_path(
    request: Request, environment_or_etag: str, path: str
) -> HTTPResponse:
    etag = get_latest_etag(environment_or_etag)
    if etag is None:
        return text(
            f"No valid etag or environment found for '{environment_or_etag}'",
            status=404,
        )
    return await view_files_in_directory(request, etag, path)


@app.route("/download/<environment_or_etag>/<path:path>", methods=["GET"])
async def download_file_path(
    request: Request, environment_or_etag: str, path: str
) -> HTTPResponse:
    etag = get_latest_etag(environment_or_etag)
    if etag is None:
        return text(
            f"No valid etag or environment found for '{environment_or_etag}'",
            status=404,
        )
    return await download_file(request, etag, path)


async def view_files_in_directory(
    request: Request, etag: str, path: str
) -> HTTPResponse:
    decoded_path = urllib.parse.unquote(path)
    directory_path = os.path.join(result_file_manager.base_workdir, etag, decoded_path)

    if not os.path.exists(directory_path):
        return text(f"Path '{etag}/{path}' not found.", status=404)

    logger.info(f"DECODED PATH: {decoded_path}")
    logger.info(f"DIRECTORY PATH: {directory_path}")
    logger.info(f"ETAG: {etag}")
    contents = []
    if os.path.isdir(directory_path):
        contents = result_file_manager.list_directory_contents(
            os.path.join(etag, decoded_path)
        )
        logger.info(f"CONTENTS: {contents}")
    else:
        return text("The specified path is not a directory.", status=400)

    html_content = f"<h1>Files in {etag}/{decoded_path}</h1><ul>"
    if decoded_path:
        html_content += '<li><a href="..">.. (go up)</a></li>'

    for entry in contents:
        entry_path = (
            os.path.join(decoded_path, entry["name"]) if decoded_path else entry["name"]
        )
        logger.info(f"Entry Path: {entry_path}")

        if entry["type"] == "file":
            download_url = urllib.parse.quote(entry_path)
            html_content += (
                f'<li>{entry["name"]} - <a href="{download_url}">Download</a></li>'
            )
        else:
            view_url = urllib.parse.quote(entry_path)
            html_content += f'<li>{entry["name"]} - <a href="{view_url}/">Open</a></li>'

    html_content += "</ul>"

    return text(html_content, headers={"Content-Type": "text/html"})


# Uncomment the listener below if you need to perform cleanup after server start
# @app.listener("after_server_start")
# async def cleanup_tmp(app, loop):
#     logger.info("Cleaning up base workdir...")
#     base_workdir = os.getenv("BASE_WORKDIR", "/tmp")
#     if os.path.exists(base_workdir):
#         for root, dirs, files in os.walk(base_workdir, topdown=False):
#             for file in files:
#                 file_path = os.path.join(root, file)
#                 try:
#                     os.remove(file_path)
#                     logger.info(f"Deleted file: {file_path}")
#                 except Exception as e:
#                     logger.error(f"Failed to delete file {file_path}: {e}")
#
#             for dir in dirs:
#                 dir_path = os.path.join(root, dir)
#                 try:
#                     shutil.rmtree(dir_path)
#                     logger.info(f"Deleted directory: {dir_path}")
#                 except Exception as e:
#                     logger.error(f"Failed to delete directory {dir_path}: {e}")


if __name__ == "__main__":
    logger.info("Starting ScriptManager Sanic server.")
    app.run(host="0.0.0.0", port=8000)
